<?php 
    require("connect_db.php");
?>
<html>
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Activity Tracker wise School</title>
    </head>
    <body>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="alert alert-info text-center"><h4>Activity tracker</h4></div>
                <div class="alert alert-success ">
                    <form class="form-inline " method="POST" action="index.php">
                        <p class="mr-sm-4 mb-4 mb-sm-0">Add new activity:</p> 
                        <input type="text" class="form-control mr-sm-4 mb-4 mb-sm-0" name="start" placeholder="Start time" size="8">
                        <input type="text" class="form-control mr-sm-4 mb-4 mb-sm-0" name="finish" placeholder="Finist time" size="8">
                        <input type="text" class="form-control mr-sm-4 mb-4 mb-sm-0" name="distance" placeholder="Distance" size="8">
                        <select class="custom-select mr-sm-4 mb-4 mb-sm-0" name="select">
                            <option selected>Select</option>
                            <option value="1">run</option>
                            <option value="2">ride</option>
                        </select>
                        <button type="submit" class="btn btn-primary mt-4 mt-sm-0 alert-dark" name="save">Save</button>
                    </form>
                    <?php
                        if(isset($_POST['save'])){
                            if(!empty($_POST['start']) and !empty($_POST['finish']) and !empty($_POST['distance'])){
                                $start=$_POST['start'];
                                $finish=$_POST['finish'];
                                $distance=$_POST['distance'];
                                $select=$_POST['select'];
                                $date=date("j F");
                                $time=(strtotime($finish)-strtotime($start))/60;
                                $sql="INSERT INTO main (activity_type, date, time, distance) VALUES ('$select', '$date', '$time','$distance')";
                                $res1=mysqli_query($connect,$sql);
                            }
                        }
                        ?>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                    <?php 
                        $sql="SELECT * FROM main";
                        $res=mysqli_query($connect,$sql);
                        while($result=mysqli_fetch_array($res)){
                    ?>
                        <div class="alert alert-warning row">
                            <?php 
                                $id=$result['id'];
                                $sql1="SELECT activity_type_table.type, main.activity_type FROM activity_type_table INNER JOIN main ON main.activity_type = activity_type_table.id_type WHERE main.id='$id'";
                                $res2=mysqli_query($connect,$sql1);
                                $result1=mysqli_fetch_array($res2);
                                $speed=round((int)$result['distance']/((int)$result['time']/60),1);
                                echo "<div class='col-sm-1'></div><div class='col-sm-2'>".$result['date']."</div>";
                                echo "<div class='col-sm-2'>".$result1['type']."</div>";
                                echo "<div class='col-sm-2'>".$result['distance']." km </div>";
                                echo "<div class='col-sm-2'>".$result['time']." m </div>";
                                echo "<div class='col-sm-3'>".$speed." km/hour </div>";
                            ?>
                        </div>
                    <?php } ?>
                    </div>
                    <div class="col-sm-4">
                        <div class="alert alert-danger text-center">
                            <b>Longest ride:</b><br>
                            <div class="row">
                                <?php 
                                    $sql_l_ride="SELECT activity_type_table.type, MAX(main.distance) AS max_d, main.time, main.date FROM activity_type_table INNER JOIN main ON main.activity_type = activity_type_table.id_type WHERE main.activity_type='2'";
                                    $res_l_ride=mysqli_query($connect,$sql_l_ride);
                                    $result_l_ride=mysqli_fetch_array($res_l_ride);
                                    echo "<div class='col-sm-4'>".$result_l_ride['date']."</div>";
                                    echo "<div class='col-sm-4'>".$result_l_ride['max_d']." km</div>";
                                    echo "<div class='col-sm-4'>".$result_l_ride['time']." m</div>";
                                ?>
                            </div>
                            <b>longest run:</b><br>
                            <div class="row">
                                <?php 
                                    $sql_l_run="SELECT activity_type_table.type, MAX(main.distance) AS max_d, main.time, main.date FROM activity_type_table INNER JOIN main ON main.activity_type = activity_type_table.id_type WHERE main.activity_type='1'";
                                    $res_l_run=mysqli_query($connect,$sql_l_run);
                                    $result_l_run=mysqli_fetch_array($res_l_run);
                                    echo "<div class='col-sm-4'>".$result_l_run['date']."</div>";
                                    echo "<div class='col-sm-4'>".$result_l_run['max_d']." km</div>";
                                    echo "<div class='col-sm-4'>".$result_l_run['time']." m </div>";
                                ?>
                            </div>
                        </div>
                        <div class="alert alert-danger">
                            <b>Total ride distance:</b>
                            <?php 
                                $sql_s_ride="SELECT SUM(main.distance) AS sum_d FROM activity_type_table INNER JOIN main ON main.activity_type = activity_type_table.id_type WHERE main.activity_type='2'";
                                $res_s_ride=mysqli_query($connect,$sql_s_ride);
                                $result_s_ride=mysqli_fetch_array($res_s_ride);
                                echo round($result_s_ride['sum_d'],1)."km";
                            ?>
                            <br><b>Total run distance:</b>
                            <?php 
                                $sql_s_run="SELECT SUM(main.distance) AS sum_d FROM activity_type_table INNER JOIN main ON main.activity_type = activity_type_table.id_type WHERE main.activity_type='1'";
                                $res_s_run=mysqli_query($connect,$sql_s_run);
                                $result_s_run=mysqli_fetch_array($res_s_run);
                                echo round($result_s_run['sum_d'],1)."km";                                    
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </body>
</html>