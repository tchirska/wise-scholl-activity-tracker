-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Час створення: Трв 26 2021 р., 14:45
-- Версія сервера: 10.4.8-MariaDB
-- Версія PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `activity_tracker`
--

-- --------------------------------------------------------

--
-- Структура таблиці `activity_type_table`
--

CREATE TABLE `activity_type_table` (
  `id_type` int(11) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `activity_type_table`
--

INSERT INTO `activity_type_table` (`id_type`, `type`) VALUES
(1, 'run'),
(2, 'ride');

-- --------------------------------------------------------

--
-- Структура таблиці `main`
--

CREATE TABLE `main` (
  `id` int(11) NOT NULL,
  `activity_type` int(11) NOT NULL,
  `date` varchar(40) NOT NULL,
  `time` varchar(40) NOT NULL,
  `distance` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп даних таблиці `main`
--

INSERT INTO `main` (`id`, `activity_type`, `date`, `time`, `distance`) VALUES
(1, 2, '26 May', '90', 12.5),
(2, 2, '26 May', '50', 5.6),
(3, 1, '26 May', '45', 5.2),
(4, 1, '26 May', '94', 10),
(5, 2, '26 May', '220', 15.5);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `activity_type_table`
--
ALTER TABLE `activity_type_table`
  ADD PRIMARY KEY (`id_type`);

--
-- Індекси таблиці `main`
--
ALTER TABLE `main`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_type` (`activity_type`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `activity_type_table`
--
ALTER TABLE `activity_type_table`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблиці `main`
--
ALTER TABLE `main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `main`
--
ALTER TABLE `main`
  ADD CONSTRAINT `main_ibfk_1` FOREIGN KEY (`activity_type`) REFERENCES `activity_type_table` (`id_type`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
